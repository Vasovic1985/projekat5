describe('Admin Demo testiranje-dodaj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')
    
    
    cy.get('li[class="nav-item"] a[href="/zgrade"]').click()
    cy.get('[id="mesto"]').clear().type('Mesto4')
    cy.get('[id="ulica"]').clear().type('Ulica4')
    cy.get('[id="broj"]').clear().type('37')
    cy.get('[id="brojStanova"]').clear().type('25')
    cy.get('[type="submit"]').click()
    cy.get('[id="toast-container"]').should('have.text',' Uspesno ste dodali zgradu! ')

   
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje-pregled', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')    
    
    cy.get('li[class="nav-item"] a[href="/zgrade"]').click()

    cy.contains('Pregled').click()
    cy.get('#prikaz').select('option[value="10"]')

    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje-pretraga po ulici', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')    
    
    cy.get('li[class="nav-item"] a[href="/zgrade"]').click()

    cy.contains('Pregled').click()
    cy.get('#ulicaBroj').click().clear().type('Ulica9')
    cy.contains('Pretraga').click()
    cy.get('.col-md-6 > a').should('contain.text','Ulica9')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje-pretraga po mestu', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')    
    
    cy.get('li[class="nav-item"] a[href="/zgrade"]').click()

    cy.contains('Pregled').click()
    cy.get('#mesto').click().clear().type('Mesto9')
    cy.contains('Pretraga').click()

    cy.get('.col-md-6 > a').should('contain.text','Mesto9')
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje-dodavanje i brisanje vlasnika i stanara', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')    
    
    cy.get('li[class="nav-item"] a[href="/zgrade"]'
    ).click()

    cy.contains('Pregled').click()
    cy.get(':nth-child(4) > .col-md-6 > a').click()
    cy.get(':nth-child(1) > td.col-md-2 > .btn').click()

    cy.get('#filter').click().clear().type('Marko')
    cy.contains('Filtriraj').click()
    cy.contains('Dodaj u stanare').click()
    cy.get('.toast-message').should('have.text',' Uspesno ste dodali stanara! ')

    cy.contains('Postavi za vlasnika').click()
    cy.get('.toast-message').should('have.text',' Uspesno ste postavili vlasnika! ')

    cy.get('#ukloniVlasnika').click()
    cy.get('.toast-message').should('have.text',' Uspesno ste uklonili vlasnika! ')

    cy.contains('Ukloni').click()
    cy.get('.toast-message').should('have.text',' Uspesno ste uklonili stanara! ')

    cy.get('.col-md-6 > a').should('contain.text','Mesto9')
    cy.contains('Izlogujte se').click()
   
    })
    
})


    describe('Admin Demo testiranje-poruke o obaveznom polju', () => {
        it('Login Test', () => {
        // valid login
        cy.signIn('admin@gmail.com', 'Bar5slova')
        
        
        cy.get('li[class="nav-item"] a[href="/zgrade"]').click()

        cy.get('[id="mesto"]').click()
        cy.get('[id="ulica"]').click()
        cy.get('[id="broj"]').click()
        cy.get('[id="brojStanova"]').click()
        cy.get('[id="broj"]').click()
       
    
        cy.get(':nth-child(1) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
    
        cy.get(':nth-child(2) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
        cy.get(':nth-child(3) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
        
        cy.get(':nth-child(4) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
       
        cy.contains('Izlogujte se').click()
       
        })
        
    }) 

    describe('Admin Demo testiranje-dodaj prazna polja', () => {
        it('Login Test', () => {
        // valid login
        cy.signIn('admin@gmail.com', 'Bar5slova')
        
        
        cy.get('li[class="nav-item"] a[href="/zgrade"]').click()

        cy.get('[id="mesto"]').click()
        cy.get('[id="ulica"]').click()
        cy.get('[id="broj"]').click()
        cy.get('[id="brojStanova"]').click()
        cy.get('[disabled=""]').should('have.text','Dodajte')
        
    
       
        cy.contains('Izlogujte se').click()
       
        })
        
    })

    describe('Admin Demo testiranje-zakljucana polja', () => {
        it('Login Test', () => {
        // valid login
        cy.signIn('admin@gmail.com', 'Bar5slova')
        
        
       
        cy.get('#opcije > :nth-child(3)').should('have.text','Institucije - zakljucano')
        cy.get('#opcije > :nth-child(4)').should('have.text','Firme -zakljucano')
        
        cy.contains('Izlogujte se').click()
       
        })
        
    })
