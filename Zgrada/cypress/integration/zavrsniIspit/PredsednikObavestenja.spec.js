describe('Predsednik Demo Testing Stranica', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.url().should('include', 'http://localhost:8080/zgrada/1/obavestenja')
    cy.get('app-navbar-zgrada > .nav > :nth-child(1) > .nav-link').should('have.text','Obavestenja')

    cy.get('.nav > :nth-child(2) > .nav-link').should('have.text','Predlozi tacke dnevnog reda')

    cy.get('.nav > :nth-child(3) > .nav-link').should('have.text','Sastanci skupstine')

    cy.get(':nth-child(4) > .nav-link').should('have.text','Kvarovi')

    cy.get('#prikaz').click().select('option[value="50"]')

    cy.get('b[_ngcontent-c6=""]').should('have.text','Dodaj')

    cy.get('[href="/pocetna"]').should('have.text','Pocetna')

    cy.get('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo Testing Add', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.contains('Stranica').click()
    cy.contains('Dodaj').click()

    cy.get('#tekstObavestenja').clear().type('Obavestenje 8')
    cy.get('[id="dodajObavestenje"]').click()

    cy.get('.toast-message').should('have.text',' Obavestenje uspesno dodato! ')
    
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo Testing Delete', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.contains('Stranica').click()
    cy.contains('brisi').click()

   

    cy.contains('.toast-message').should('have.text',' Uspesno izbrisano obavestenje! ')
    
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo Testing Edit', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get('Stranica').click()
    cy.get('izmeni').click()
    cy.get('#noviTekst').clear().type("Izmenjeni tekst 1")
    cy.get('potvrdi').click()

   

    cy.contains('.toast-message').should('have.text',' Uspesno izmenjeno obavestenje! ')
    
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

