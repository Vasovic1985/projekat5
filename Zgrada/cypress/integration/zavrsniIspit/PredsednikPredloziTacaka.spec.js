describe('Predsednik Demo testiranje Predlozi tacke dnevnog reda dodaj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.get(':nth-child(4) > a').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('Dodaj').click()
    cy.get('#tekstPtdr').click().clear().type('Novi predlog 1')
    cy.get('.center > .btn').click()

    cy.get('.toast-message').should('have.text',' Predlog tacke dnevnog reda uspesno dodat ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje Predlozi tacke dnevnog reda prikaz', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.get(':nth-child(4) > a').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('#prikaz').click().select('option[value="50"]')
    

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje Predlozi tacke dnevnog reda izmeni', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.get(':nth-child(4) > a').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('izmeni').click()
    cy.contains('#noviTekst').click().clear().type('Izmenjeni tekst 1')
    cy.contains('potvrdi').dblclick()

    cy.get('.toast-message').should('have.text',' Tacka uspesno izmenjena ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje Predlozi tacke dnevnog reda brisi', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.get(':nth-child(4) > a').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('brisi').click()
   

    cy.get('.toast-message').should('have.text',' Tacka uspesno izbrisana ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje Predlozi tacke dnevnog reda izaberi skupstinu', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')

    cy.get(':nth-child(4) > a').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('.custom-select').click().select('option[value="1: Object"]')


    

    cy.contains('Izlogujte se').click()
   
    })
    
})
