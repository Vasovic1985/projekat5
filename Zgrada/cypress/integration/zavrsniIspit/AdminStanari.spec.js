describe('Admin Demo testiranje -stanari-dodavanje', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')

    cy.contains('Stanari').click()

    cy.get('#email').click().clear().type('markokolic@gmail.com')
    cy.get('#lozinka').click().clear().type('Bar10slova')
    cy.get('#ime').click().clear().type('Marko')
    cy.get('#prezime').click().clear().type('Nikolic')

    cy.get('.col-lg-9 > .btn-primary').click()

    cy.get('.toast-message').should('have.text',' Uspesno ste registrovali stanara! ')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje -stanari-pretraga', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')
    cy.get(':nth-child(3) > .nav-link').click()
    cy.get(':nth-child(2) > .btn > b').click()
    

    cy.get('#filter').click().clear().type('Janko')
    cy.contains('Pretraga').click()

    cy.get('.col-md-11 > a').should('contain.text','Janko')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Admin Demo testiranje -stanari-padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')

    cy.get(':nth-child(3) > .nav-link').click()
    cy.get(':nth-child(2) > .btn > b').click()

    cy.get('#prikaz').select('option[value="25"]')

    
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje -stanari-resetovanje', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')

    cy.contains('Stanari').click()

    cy.get('#email').click().clear().type('peranikolic@gmail.com')
    cy.get('#lozinka').click().clear().type('Bar8slova')
    cy.get('#ime').click().clear().type('Pera')
    cy.get('#prezime').click().clear().type('Nikolic')

    cy.contains('Resetujte').click()

    cy.get('#email').should('have.text','')
    cy.get('#lozinka').should('have.text','')
    cy.get('#ime').should('have.text','')
    cy.get('#prezime').should('have.text','')

    
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje -stanari-poruka o obaveznom unosu', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')

    cy.contains('Stanari').click()

    cy.get('#email').click()
    cy.get('#lozinka').click()
    cy.get('#ime').click()
    cy.get('#prezime').click()
    cy.get('#ime').click()

    cy.get(':nth-child(1) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
    
    cy.get(':nth-child(2) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
    cy.get(':nth-child(3) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
    
    cy.get(':nth-child(4) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
   

      
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Admin Demo testiranje -stanari-dodavanje bez podataka', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('admin@gmail.com', 'Bar5slova')

    cy.contains('Stanari').click()

    cy.get('#email').click()
    cy.get('#lozinka').click()
    cy.get('#ime').click()
    cy.get('#prezime').click()

    cy.get('[disabled=""]').should('have.text','Registrujte')

   
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

