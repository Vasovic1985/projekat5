describe('Stanar Demo testiranje-sastanci skupstine-pocetak i kraj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Sastanci skupstine').click()

    cy.contains('Pregledaj tacke').click()

    cy.get(':nth-child(2) > td > .low-impact-text').should('contain.text','Pocetak skupstine')
    cy.get(':nth-child(3) > td > .low-impact-text').should('contain.text','Kraj skupstine')
   

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-sastanci skupstine padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Sastanci skupstine').click()

    cy.get('#prikaz').select('option[value="10"]')

   

    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Stanar Demo testiranje-sastanci skupstine druga padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Sastanci skupstine').click()

    cy.get('.custom-select ng-pristine ng-valid ng-touched').select('option[value="2: 1"]')

   

    cy.contains('Izlogujte se').click()
   
    })
    
})

