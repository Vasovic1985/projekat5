describe('Predsednik Demo testiranje kvarova-padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Kvarovi').click()
    cy.get('#prikaz').click().select('option[value="10"]')
    
   cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje kvarova-dodaj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Kvarovi').click()
    cy.contains('Dodaj').click()

    cy.get('#mesto').click().clear().type('mesto 1')
    cy.get('#opis').click().clear().type('opis 1')
    cy.get('#odgovorno_lice').click()
    cy.get('.col-form-label > .form-control').click().clear().type('Marko')
    cy.get('#button_3').dblclick()
    cy.get('#submit').click()

    cy.get('.toast-message').should('have.text',' Kvar uspesno dodat ')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Predsednik Demo testiranje kvarova-prikazi zavrsene kvarove', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Kvarovi').click()
    

    cy.contains('.label > .ng-untouched').should('have.text',' Prikazi zavrsene kvarove')
    
    cy.contains('.label > .ng-untouched').click()
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje kvarova-obrisi kvar', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Kvarovi').click()
    
    cy.contains('brisi').click()

    cy.get('.toast-message').should('have.text',' Uspesno izbrisan kvar ')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})
