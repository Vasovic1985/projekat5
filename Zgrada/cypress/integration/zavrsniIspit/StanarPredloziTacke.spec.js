describe('Stanar Demo testiranje-dodaj predloge tacaka', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('Dodaj').click()

    cy.get('#tekstPtdr').click().clear().type('Obavestenje o necemu 1')

    cy.contains('Potvrdi').click()

    cy.get('.toast-message').should('have.text',' Predlog tacke dnovnog reda uspesno dodat! ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-izmeni predloge tacaka', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.get('#izmeniObavestenje').click()

    cy.get('#noviTekst').clear().type('Izmenjeni prelozi tacaka 1')

    cy.contains('potvrdi').click()

    cy.get('.toast-message').should('have.text',' Predlog tacke dnovnog reda uspesno izmenjen! ')

    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Stanar Demo testiranje-odustani od izmene predloga tacaka', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.get('#izmeniObavestenje').click()

    cy.get('#noviTekst').clear().type('Izmenjeni prelozi tacaka 1')

    cy.contains('odustani').click()

    

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-brisi predloge tacaka', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Predlozi tacke dnevnog reda').click()
    cy.contains('brisi').click()

    
    cy.get('.toast-message').should('have.text',' Predlog tacke dnovnog reda uspesno obrisan! ')

    cy.contains('Izlogujte se').click()
   
    })
    
})
