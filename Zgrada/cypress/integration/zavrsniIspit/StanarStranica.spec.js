describe('Stanar Demo testiranje-dodaj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.get('#dodajObavestenje').click()

    cy.get('#tekstObavestenja').click().clear().type('Obavestenje o necemu 4')

    cy.get('#dodajObavestenje').click()

    cy.get('.toast-message').should('have.text',' Obavestenje uspesno dodato! ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.get('#prikaz').select('option[value="10"]')

    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-izmeni', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.get('#izmeniObavestenje').click()

    cy.get('#noviTekst').click().clear().type('Obavestenje o necemu izmenjeno 1')

    cy.contains('potvrdi').click()

    cy.get('.toast-message').should('have.text',' Uspesno izmenjeno obavestenje ')

    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Stanar Demo testiranje-brisi', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('brisi').click()

    

    cy.get('.toast-message').should('have.text',' Uspesno izbrisano obavestenje ')

    cy.contains('Izlogujte se').click()
   
    })
    
})


describe('Stanar Demo testiranje-odustani', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.get('#izmeniObavestenje').click()

    cy.get('#noviTekst').click().clear().type('Obavestenje o necemu izmenjeno 1')

    cy.contains('odustani').click()

   

    cy.contains('Izlogujte se').click()
   
    })
    
})

