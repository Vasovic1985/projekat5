describe('Stanar Demo testiranje-kvarovi-dodaj', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.contains('Dodaj').click()

    cy.get('#mesto').click().clear().type('Neko mesto 1')
    cy.get('#opis').click().clear().type('Nesto sto opisuje mesto 1')
    cy.get('#odgovorno_lice').click()
    cy.get('#button_2').click()
    cy.get('#submit').click()
    
    cy.get('.toast-message').should('have.text',' Kvar uspesno dodat ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-kvarovi-dodaj prazno i proveri poruke', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.contains('Dodaj').click()

    cy.get('#mesto').click()
    cy.get('#opis').click()
    cy.get('#mesto').click()

    cy.get(':nth-child(1) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')
    cy.get(':nth-child(2) > .col-md-9 > .invalid-feedback').should('have.text','Ovo polje ne sme biti prazno!')

    cy.get('[disabled=""]').should('have.text','Dodaj')
    
    
   
    cy.contains('Izlogujte se').click()
   
    })

    
    
})

describe('Stanar Demo testiranje-kvarovi-dodaj provera pretrage', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.contains('Dodaj').click()

    cy.get('#mesto').click().clear().type('Neko mesto 1')
    cy.get('#opis').click().clear().type('Nesto sto opisuje mesto 1')

    cy.get('#odgovorno_lice').click()

    cy.get('.form-control ng-pristine ng-valid ng-touched').click().clear().type('Predsednik')
    cy.get('#button_2').click()
    cy.get('#submit').click()
    
    cy.get('.toast-message').should('have.text',' Kvar uspesno dodat ')

    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Stanar Demo testiranje-kvarovi-brisi', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.contains('brisi').click()

        
    cy.get('.toast-message').should('have.text',' Uspesno izbrisan kvar ')

    cy.contains('Izlogujte se').click()
   
    })
})


describe('Stanar Demo testiranje-kvarovi-padajuca lista', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.get('#prikaz').select('option[value="10"]')        
   

    cy.contains('Izlogujte se').click()
   
    })
})


describe('Stanar Demo testiranje-kvarovi-prikazi zavrsene kvarove', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Stranica').click()
    cy.contains('Kvarovi').click()

    cy.get('label > .ng-untouched').click().should('be.checked')        
   

    cy.contains('Izlogujte se').click()
   
    })
})



