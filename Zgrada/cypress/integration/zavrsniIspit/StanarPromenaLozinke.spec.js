describe('Stanar Demo testiranje-promena lozinke', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('marko@gmail.com', 'Bar5slova')

    cy.contains('Promena lozinke').click()
    cy.get('#staraLozinka').click().clear().type('Bar5slova')
    cy.get('#novaLozinka').click().clear().type('Bar5slova')
    cy.get('#potvrdaNoveLozinke').click().clear().type('Bar5slova')

   

    cy.contains('Promenite lozinku').click()

    cy.get('.toast-message').should('have.text',' Lozinka uspesno izmenjena! ')

    
   
    })
    
})

