describe('Predsednik Demo testiranje sastanaka skuspstine', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Sastanci skupstine').click()
    cy.contains('Dodaj').click()
    cy.get('[[name="pocetakSkupstine"]]').click().type('12/16/2020,8:45PM')


    cy.get('[name="zavrsetakSkupstine"]').click().clear().type('12/16/2020,11:45PM')
    cy.contains('.center > .btn').click()
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje sastanaka skuspstine padajuce liste', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Sastanci skupstine').click()
    cy.contains('#prikaz').click().select('optina[value="25"]')
    cy.contains('.custom-select').click().select('optina[value="2: 1"]')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})

describe('Predsednik Demo testiranje sastanaka skuspstine pregledaj tacke', () => {
    it('Login Test', () => {
    // valid login
    cy.signIn('predSkup@gmail.com', 'Bar5slova')
    cy.get(':nth-child(4) > a').click()

    cy.contains('Sastanci skupstine').click()
    cy.contains('Pregledaj tacke').click()
    cy.contains('#prikaz').click().select('option[value="10"]')
    
    cy.contains('Izlogujte se').click()
   
    })
    
})